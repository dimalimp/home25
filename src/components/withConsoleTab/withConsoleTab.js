import React, { Component } from 'react';

const withConsoleTab = (Wrapped, name) => {
    class HOC extends Component {
        componentDidMount() {
            console.log(name)
        }
        render() {
            return (
                <Wrapped { ...this.props }
                />
            );
        }
    }
    return HOC;
};

export default withConsoleTab;
