import { Fragment } from 'react';
import "./Cars.scss"
import withConsoleTab from '../withConsoleTab/withConsoleTab';

const Cars = () => {
    return (
        <Fragment>
            <h2 className="cars">В наличии</h2>
            <ul>
                <li className="cars__item">БМВ</li>
                <li className="cars__item">Мерседес</li>
                <li className="cars__item">Ауди</li>
                <li className="cars__item">Форд</li>
                <li className="cars__item">Опель</li>
            </ul>
        </Fragment>
    )
}

export default withConsoleTab(Cars, "машины");