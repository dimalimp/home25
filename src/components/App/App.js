import { Fragment } from 'react';
import './App.scss';
import Intro from '../Intro/Intro';

const App = () => {
  return (
    <Fragment>
      <Intro />
    </Fragment>
  );
}

export default App;
