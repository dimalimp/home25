import './Intro.scss';
import { Link, Switch, Route } from "react-router-dom";
import Cars from "../Cars/Cars"
import Chandeliers from "../Chandeliers/Chandeliers";
import Welcome from "../Welcome/Welcome";


const Intro = () => {
    return (
        <section className="intro">
            <div className="intro__container">
                <Link to="/"><h1 className="intro__title">Мир машин и люстр</h1></Link>
                <div className="intro__inner">
                    <div className="intro__nav">
                        <Link className="intro__link" to="/cars" >Машины</Link>
                        <Link className="intro__link" to="/chandeliers">Люстры</Link>
                    </div>
                    <div className="intro__content">
                        <Switch>
                            <Route exact path="/">
                                <Welcome />
                            </Route>
                            <Route path="/cars">
                                <Cars />
                            </Route>
                            <Route path="/chandeliers">
                                <Chandeliers />
                            </Route>
                        </Switch>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Intro;