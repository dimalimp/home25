import "./Chandeliers.scss"
import withConsoleTab from "../withConsoleTab/withConsoleTab";

const Chandeliers = () => {
    return (
        <div>
            <h2 className="chandeliers">В наличии</h2>
            <ul>
                <li className="chandeliers__item">Классические люстры</li>
                <li className="chandeliers__item">Кованые люстры</li>
                <li className="chandeliers__item">Хрустальные люстры</li>
            </ul>
        </div>
    )
}

export default withConsoleTab(Chandeliers, "люстры");